package model.node;

/**
 * Created by Yusuf on 21-Mar-17.
 */
public enum ConnectionStatus {
    CONNECTED, NOT_CONNECTED;
}
