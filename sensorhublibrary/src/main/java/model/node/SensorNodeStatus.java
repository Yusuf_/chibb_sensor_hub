package model.node;

/**
 * Created by Yusuf on 08-Mar-17.
 */
public enum SensorNodeStatus {
    OK, INTERMITTENT_FAILURE, DOWN
}
