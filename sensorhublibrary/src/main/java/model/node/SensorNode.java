package model.node;

import lombok.Data;
import lombok.NoArgsConstructor;
import model.sensor.SensorDataType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yusuf on 08-Mar-17.
 */
@Data
@Entity
@NoArgsConstructor
public class SensorNode implements Serializable{
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String name;
    private String ip;
    private String topic;
    @Enumerated(EnumType.STRING)
    private SensorNodeStatus status = SensorNodeStatus.OK;
    @Enumerated(EnumType.STRING)
    private SensorDataType type;
    @Enumerated(EnumType.STRING)
    private ConnectionStatus connectionStatus = ConnectionStatus.NOT_CONNECTED;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name="owner", referencedColumnName="id")
    private List<SensorNodeFailure> failures = new ArrayList<>();

}
