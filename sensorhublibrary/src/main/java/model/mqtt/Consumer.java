package model.mqtt;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.node.SensorNode;
import model.node.SensorNodeFailure;
import model.sensor.SensorData;
import model.sensor.SensorReading;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.http.HttpEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Yusuf on 10-Mar-17.
 */
@Slf4j
public class Consumer implements MqttCallback {
    @Getter
    private MqttClient client;
    private SensorNode sensorNode;
    private PagingAndSortingRepository<SensorData,String> repository;

    private ObjectMapper mapper = new ObjectMapper();
    private SimpMessagingTemplate template;

    public boolean connect(){
        try {
            this.client = new MqttClient("tcp://" + sensorNode.getIp() + ":9003", sensorNode.getName(), new MemoryPersistence());
            final MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
            String lastWill = "chibb/last_will_topic";
            String lastWillBody = "MQTT Broker Offline";
            int qosNumber = 2;
            mqttConnectOptions.setWill(lastWill, lastWillBody.getBytes(), qosNumber, true);
            client.setTimeToWait(7000L);
            client.connect(mqttConnectOptions);
            client.setCallback(this);
            client.subscribe(sensorNode.getTopic());
        } catch (MqttException e) {
            log.info("==== " + e.getMessage() + " ====");
        }
        return client.isConnected();
    }

    @Override
    public void connectionLost(Throwable throwable) {
        SensorNodeFailure failure = new SensorNodeFailure();
        Optional<String> optional = Optional.ofNullable(throwable.getCause().getMessage());
        String message = optional.orElse("MQTT Client Is Down");
        failure.setCause(message);
        failure.setTimestamp(LocalDateTime.now(ZoneId.systemDefault()));

        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<SensorNodeFailure> request = new HttpEntity<>(failure);
        restTemplate.postForObject("http://localhost:9004/sensors/failure/" + sensorNode.getId(), request, SensorNodeFailure.class);
        log.info("===== Failure Sent ====");
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        String payLoad = new String(mqttMessage.getPayload(), "UTF-8");
        mapper.findAndRegisterModules();
        SensorData sensorData = mapper.readValue(payLoad, SensorData.class);
        sensorData.setSensorNodeId(sensorNode.getId());
        List<SensorReading> readings = sensorData.getReading().stream()
                .filter(sensorReading -> sensorReading.getType() == sensorNode.getType())
                .collect(Collectors.toList());
        if (!readings.isEmpty()){
            sensorData.setReading(readings);
            log.info("====== Received: " + readings.size() + " items - ID " + sensorNode.getName() +  " =====");
            template.convertAndSend("/topic/sensor-data", sensorData);
            repository.save(sensorData);
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    public Consumer(SensorNode sensorNode,
                    PagingAndSortingRepository<SensorData, String> repository,
                    SimpMessagingTemplate template) {
        this.sensorNode = sensorNode;
        this.repository = repository;
        this.template = template;
    }
}
