package model.sensor;

/**
 * Created by Yusuf on 10-Mar-17.
 */
public enum SensorDataType {
    TEMPERATURE, DENSITY, PROXIMITY, LIGHT, PRESSURE
}
