package nl.hro;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Yusuf on 08-May-17.
 */
@Entity
@Data
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="role_id")
    private int id;
    private String role;
}
