package nl.hro;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by Yusuf on 08-May-17.
 */
@Slf4j
@RestController
public class LoginController {
    @Autowired
    private UserService userService;
    @RequestMapping(value={"/", "/login"}, method = RequestMethod.GET)
    public String login(){
        return  "Trying to log in";
    }

    @RequestMapping(value={"/user"}, method = RequestMethod.GET)
    public User getUSer(){
        log.info("Trying to get user");
        User user = userService.findUserByEmail("jd@gmail.com");
        if (user == null) {
            user = new User();
            log.info("Not Found!");
        }
        else
            log.info("Found!");
        return user;
    }


    @RequestMapping(value="/registration", method = RequestMethod.GET)
    public String registration(){
        return "Trying to register";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public User createNewUser(@RequestBody @Valid User user, BindingResult bindingResult) {
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            log.info("Errors: " + bindingResult.toString());
        } else {
            userService.saveUser(user);
            log.info("Saved: " + user);
        }
        return user;
    }

    @RequestMapping(value="/admin/home", method = RequestMethod.GET)
    public User home(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        return user;
    }
}
