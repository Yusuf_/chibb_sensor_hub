package nl.hro;

/**
 * Created by Yusuf on 08-May-17.
 */
public interface UserService {
    public User findUserByEmail(String email);
    public void saveUser(User user);
}