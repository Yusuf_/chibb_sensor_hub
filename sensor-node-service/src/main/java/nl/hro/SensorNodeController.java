package nl.hro;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import model.mqtt.Consumer;
import model.node.ConnectionStatus;
import model.node.SensorNode;
import model.node.SensorNodeFailure;
import model.node.SensorNodeStatus;
import model.sensor.SensorData;
import model.sensor.SensorReading;
import nl.hro.repository.RestResponse;
import nl.hro.repository.SensorDataRepository;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Yusuf on 08-Mar-17.
 */
@RequestMapping("/sensors")
@RestController
@Slf4j
@EntityScan("model.node")
@CrossOrigin(origins = "http://localhost:4200")
@Api(value="Sensor Node Management", description="Handle all the operations concerning the sensor nodes")
public class SensorNodeController {

    @Autowired
    private SensorNodeRepository sensorNodeRepository;

    @Autowired
    private SensorDataRepository sensorDataRepository;

    private Map<Long, Consumer> activeNodes = new HashMap<>();

    @Autowired
    SimpMessagingTemplate template;

    @ApiOperation(value = "Disconnect a sensor node", response = RestResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully disconnected"),
    }
    )
    @RequestMapping(path = "/disconnect/{id}", method = RequestMethod.POST)
    public ResponseEntity<RestResponse> disconnectNode(@PathVariable Long id){
        SensorNode node = sensorNodeRepository.findOne(id);
        RestResponse status = disconnect(node);
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @ApiOperation(value = "Connect a sensor node", response = SensorNode.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully connected sensor node"),
    }
    )
    @RequestMapping(path = "/connect/{id}", method = RequestMethod.POST)
    public ResponseEntity connectToNode(@PathVariable Long id){
        RestResponse connectionStatus = new RestResponse();
        SensorNode node = sensorNodeRepository.findOne(id);

        if(!activeNodes.containsKey(id)){
            log.info("==== Connecting: "  + node.getName() + " ====");
            connectionStatus = connect(node);
        } else {
            log.info("==== Already Connected: "  + node.getName() + " ====");
        }
        return new ResponseEntity<>(connectionStatus,HttpStatus.OK);
    }

    @ApiOperation(value = "Update information of sensor node", response = RestResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated sensor node information"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @RequestMapping(path = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody SensorNode node, @PathVariable Long id){
        log.info("==== Updating ====");
        RestResponse restResponse = new RestResponse(true);
        Optional<Consumer> consumerOptional = Optional.ofNullable(activeNodes.get(id));

        consumerOptional
                .ifPresent(consumer -> {
                    log.info("==== Updating A Connected Node ====");
                    RestResponse disconnect = disconnect(node);
                    restResponse.setStatus(disconnect.isStatus());

                });
        sensorNodeRepository.save(node);
        return new ResponseEntity<>(restResponse, HttpStatus.OK);
    }

    @ApiOperation(value = "Retrieve list of sensor data of a sensor node", response = SensorReading.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @RequestMapping(path = "/data/{id}", method = RequestMethod.GET)
    public HttpEntity<List<SensorReading>> sensorNodeData
            (@PathVariable Long id,
             @RequestParam(value="date")@DateTimeFormat(pattern = "yyyy-MM-dd-HH:mm:ss") LocalDateTime timestamp){

        SensorNode sensorNode = sensorNodeRepository.findOne(id);
        log.info("==== Getting data from " + sensorNode.getName() +" ====");
        List<SensorData> sensorData = sensorDataRepository.findBySensorNodeId(id);
        List<SensorReading> sensorReadings = sensorData.stream()
                .flatMap(sensorData1 -> sensorData1.getReading().stream())
                .filter(sensorReading -> sensorReading.getTimestamp().isAfter(timestamp))
                .collect(Collectors.toList());

        log.info("==== Total Found " + sensorData.size() + " ====");
        log.info("==== Total Filtered " + sensorReadings.size() + " ====");

        return new ResponseEntity<>(sensorReadings,HttpStatus.OK);
    }

    @ApiOperation(value = "View a list of connected sensor nodes", response = SensorNode.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @RequestMapping(path = "/active", method = RequestMethod.GET)
    public HttpEntity<Iterable<SensorNode>> getActiveNodes(){
        Iterable<SensorNode> nodes = sensorNodeRepository.findAll(this.activeNodes.keySet());
        log.info("==== Active Sensor Nodes " + nodes + " ====");
        return new ResponseEntity<>(nodes, HttpStatus.OK);
    }

    @ApiOperation(value = "Register a failure of a sensor node", response = SensorNode.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added failure"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
    @RequestMapping(path = "/failure/{id}", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public SensorNode registerFailure(@PathVariable Long id, @RequestBody SensorNodeFailure failure){
        LocalDateTime now = LocalDateTime.now(ZoneId.of("Europe/Amsterdam")).minusMinutes(5);
        SensorNode sensorNode = sensorNodeRepository.findOne(id);
        log.info("==== Adding Failure For " + sensorNode.getName() + " =====");
        List<SensorNodeFailure> failures = sensorNode.getFailures();
        failures.add(failure);

        int totalFailures = failures.stream()
                .filter(sensorNodeFailure -> sensorNodeFailure.getTimestamp().isAfter(now))
                .mapToInt(value -> 1)
                .sum();

        if (totalFailures >= 3 && totalFailures <= 10){
            sensorNode.setStatus(SensorNodeStatus.INTERMITTENT_FAILURE);
        } else if (totalFailures >= 10){
            sensorNode.setStatus(SensorNodeStatus.DOWN);
        } else {
            sensorNode.setStatus(SensorNodeStatus.OK);
        }
        sensorNode.setFailures(failures);
        log.info("==== Total Failures: " + totalFailures + " ====");
        activeNodes.remove(id);
        sensorNodeRepository.save(sensorNode);
        return sensorNode;
    }

    private RestResponse connect(SensorNode sensorNode){

        Consumer consumer = new Consumer(sensorNode,sensorDataRepository, template);
        log.info("==== Trying to connect: " + sensorNode.getName() + " =====");
        boolean connected = consumer.connect();
        if(connected){
            log.info("==== Connected: " + sensorNode.getName() + " ====");
            sensorNode.setConnectionStatus(ConnectionStatus.CONNECTED);
            activeNodes.put(sensorNode.getId(),consumer);
        } else {
            log.info("===== Connecting Failed: " + sensorNode.getId() + " =====");
            SensorNodeFailure sensorNodeFailure = new SensorNodeFailure();
            sensorNodeFailure.setTimestamp(LocalDateTime.now(ZoneId.of("Europe/Amsterdam")));
            sensorNodeFailure.setCause("Connecting to MQTT broker failed");
            sensorNode.getFailures().add(sensorNodeFailure);
            sensorNode.setConnectionStatus(ConnectionStatus.NOT_CONNECTED);
        }
        sensorNodeRepository.save(sensorNode);

        return new RestResponse(connected);
    }

    private RestResponse disconnect(SensorNode node){
        log.info("==== Disconnecting: " + node.getId() + " ====");
        Optional<Consumer> consumerOptional = Optional.ofNullable(activeNodes.get(node.getId()));
        RestResponse restResponse = new RestResponse();
        consumerOptional.ifPresent(consumer -> {
            try {
                consumer.getClient().disconnect();
                activeNodes.remove(node.getId());
                node.setConnectionStatus(ConnectionStatus.NOT_CONNECTED);
                restResponse.setStatus(true);
                log.info("==== Disconnecting Succeeded ====");
            } catch (MqttException e) {
                SensorNodeFailure failure = new SensorNodeFailure();
                Optional<String> optional = Optional.ofNullable(e.getMessage());
                String message = optional.orElse("Disconnecting failed");
                failure.setCause(message);
                node.getFailures().add(failure);
                restResponse.setStatus(false);
                log.info("==== Disconnecting Failed: " + e.getMessage() + " ====");
            }
        });

        sensorNodeRepository.save(node);
        return restResponse;
    }
}
