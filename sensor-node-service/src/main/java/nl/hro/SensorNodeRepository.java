package nl.hro;

import model.node.SensorNode;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Yusuf on 08-Mar-17.
 */
public interface SensorNodeRepository extends CrudRepository<SensorNode,Long> {
}
