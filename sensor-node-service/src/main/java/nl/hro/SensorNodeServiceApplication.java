package nl.hro;

import model.node.SensorNode;
import model.sensor.SensorDataType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
@SpringBootApplication
public class SensorNodeServiceApplication {

	@Autowired
	SensorNodeRepository sensorNodeRepository;

	public static void main(String[] args) {
		SpringApplication.run(SensorNodeServiceApplication.class, args);
	}
	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true); // you USUALLY want this
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("PUT");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		SensorNode sensorNode = new SensorNode();
		sensorNode.setIp("145.24.222.65");
		sensorNode.setTopic("/home/temperature");
		sensorNode.setName("CLR");
		sensorNode.setType(SensorDataType.LIGHT);


		return args -> {
			sensorNodeRepository.save(sensorNode);
		};
	}
}
