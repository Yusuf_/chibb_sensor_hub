package nl.hro.repository;

import model.sensor.SensorData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Yusuf on 18-Mar-17.
 */
@RepositoryRestResource(collectionResourceRel = "data", path = "data")
public interface SensorDataRepository extends MongoRepository<SensorData, String> {
    List<SensorData> findBySensorNodeId(Long id);

}
