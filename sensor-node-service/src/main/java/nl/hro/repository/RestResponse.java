package nl.hro.repository;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Yusuf on 23-Mar-17.
 */
@Data
@NoArgsConstructor
public class RestResponse {
    private boolean status;

    public RestResponse(boolean result){
        this.status = result;
    }
}
