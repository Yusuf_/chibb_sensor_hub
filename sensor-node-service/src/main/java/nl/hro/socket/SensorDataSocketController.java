package nl.hro.socket;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Yusuf on 24-Mar-17.
 */
@RestController
public class SensorDataSocketController {
    @MessageMapping("/sensor-data")
    public String greeting(String date) throws Exception {
        return date;
    }
}
