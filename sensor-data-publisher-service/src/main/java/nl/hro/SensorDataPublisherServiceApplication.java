package nl.hro;

import nl.hro.MQTT.SensorDataPublisher;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.UnsupportedEncodingException;
@EnableEurekaClient
@EnableScheduling
@SpringBootApplication
public class SensorDataPublisherServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SensorDataPublisherServiceApplication.class, args);

	}

	@Autowired
	private SensorDataPublisher publisher;

	@Scheduled(fixedRate = 5000)
	public void sendData() throws MqttException, UnsupportedEncodingException {
		publisher.publishData();
	}
}
