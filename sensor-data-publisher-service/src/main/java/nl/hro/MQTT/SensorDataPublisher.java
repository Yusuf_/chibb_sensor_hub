package nl.hro.MQTT;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import model.sensor.SensorData;
import model.sensor.SensorDataType;
import model.sensor.SensorDataUnit;
import model.sensor.SensorReading;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Yusuf on 23-Feb-17.
 */
@Slf4j
@Controller
public class SensorDataPublisher implements MqttCallback {

    @Value("${mqtt.client-id}")
    private String CLIENT_ID;

    @Value("${mqtt.topic}")
    private String TOPIC;

    @Value("${mqtt.server.uri}")
    private String SERVER_URI;
    private MqttClient client;

    public SensorDataPublisher(){
    }

    @PostConstruct
    public void setup(){
        try {
            client = new MqttClient(SERVER_URI, CLIENT_ID);
            client.connect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    private SensorData createRandomData(){
        Random random = new Random();
        SensorData sensorData = new SensorData();
        sensorData.setUuid(UUID.randomUUID().toString());
        sensorData.setLocation("HOME");
        sensorData.setBattery(random.nextInt(101));
        SensorDataType[] types = SensorDataType.values();
        SensorDataUnit[] units = SensorDataUnit.values();
        List<SensorReading> readings = new ArrayList<>();
        sensorData.setTimestamp(LocalDateTime.now(ZoneId.of("Europe/Amsterdam")));
        for (int i = 0; i < random.nextInt(6); i++) {
            int randomNumber = new Random().nextInt(10);
            SensorReading sensorReading = new SensorReading();
            sensorReading.setTimestamp(LocalDateTime.now(ZoneId.of("Europe/Amsterdam")).minusMinutes(randomNumber));
            sensorReading.setReading(random.nextFloat() * 10);
            int typePosition = (i+randomNumber) % types.length;
            int unitPosition = (i+randomNumber) % units.length;
            sensorReading.setType(types[typePosition]);
            sensorReading.setUnit(units[unitPosition]);
            readings.add(sensorReading);
        }
        sensorData.setReading(readings);
        return sensorData;
    }


    public void publishData() throws MqttException, UnsupportedEncodingException {
        ObjectMapper mapper = new ObjectMapper();
        SensorData randomData = createRandomData();
        try {
            byte[] messagePayload = mapper.writeValueAsBytes(randomData);
            MqttMessage message = new MqttMessage(messagePayload);
            message.setQos(2);
            message.setRetained(false);
            log.info("==== Sending: " + message + " =====");
            client.publish(TOPIC, message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }



    @Override
    public void connectionLost(Throwable throwable) {

    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        log.info("==== Sent Data ====");
    }
}
