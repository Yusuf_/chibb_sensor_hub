# ICTLab (CHIBB) - Sensor Data Hub 

![N|Solid](http://www.esfa.nl/escooter010/HRO.gif)

In het CHIBB concept huis bevinden zich diverse sensor nodes die data verzamelen over temperatuur, luchtvochtigheid etc. De sensor nodes stellen die data beschikbaar via een data stream. 

De sensor data hub is een systeem dat alle data van de sensor nodes ontvangt en weergeeft op een manier dat bezoekers en inwoners inzicht krijgen in hoe een duurzaam huis werkt. Hierbij kun jij denken aan:

 - Grafieken tonen die real-time data ontvangen
 - Grafieken van historische data
 - Interactieve grafieken

# Architectuur
![N|Solid](https://lh4.googleusercontent.com/OOWGqRCXRVclWlEsukj7JETYA1bllM33YwKXQoEFpoRbS8sU3HMaZf5nBg6TVO6SMxdDDYIAWMJacsQ=w1360-h659)

Hierboven is er een diagram van hoe de applicatie momenteel er uit ziet (23-02-2017). De sensor hub wordt gebouwd volgens de microservices architectuur. De services die momenteel gerealiseerd zijn:
 - Sensor Data MQTT Publisher
 - Sensor Data API
 - Sensor Data Socket
 
# Microservices

**Sensor Data MQTT Publisher**
Deze service is verantwoordelijk voor het ontvangen van de data en doorsturen naar de MQTT broker. De sensor nodes zijn momenteel nog niet beschikbaar. De data stream wordt gesimuleerd door zelf om de drie seconden nieuwe objecten te maken en te publishen op de onderstaande MQTT topic.

```sh
 /home/temperature
```

```sh
 {
    "timestamp" : "2017-01-01T15:34:12",
    "temperature" : 7.0254256,
    "location" : "living-room"
}
```

**Sensor Data API**
Deze service abonneert zich op de topic die hierboven is vermeld. Zodra het berichten binnenkrijgt dan zet het om naar eigen domein model in Java.

```java
public class Temperature {
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalDateTime title;
    private double temperature;
}
```

Het Java object wordt vervolgens opgeslagen in een MongoDB. Er is een REST API beschikbaar om met de database te communiceren. Hieronder is de URL te zien en welke opties je kunt mee geven aan je HTTP request.

Request:
```sh
$ curl http://localhost:8080
```
Response:
```javascript
{
  "_links" : {
    "people" : {
      "href" : "http://localhost:8080/temperatures{?page,size,sort}",
      "templated" : true
    }
  }
}
```

Hieronder is te zien als je alle data van de database wilt opvragen. Alle objecten bevatten ook een link naar waar je het kunt vinden. Het komt doordat dit een Hypermedia-Driven Restful service is.

Request:
```sh
$ curl http://localhost:8080/temperatures
```
Response:
```javascript
{
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/people{?page,size,sort}",
      "templated" : true
    },
    "search" : {
      "href" : "http://localhost:8080/people/search"
    }
  },
  "_embedded" : {
    "temperatures" : [ {
      "timestamp" : "2017-01-01T15:34:12",
      "location" : "living-room",
      "_links" : {
        "self" : {
          "href" : "http://localhost:8080/temperatures/53143004990b1af9f229"
        }
      }
    } ]
  },
  "page" : {
    "size" : 20,
    "totalElements" : 1,
    "totalPages" : 1,
    "number" : 0
  }
}
```

**Sensor Data Socket**
De service krijgt berichten binnen van de MQTT broker en verstuurt via WebSocket door naar de UI.

Stel een STOMP endpoint beschikbaar waar UI zich aan kan verbinden:
```java
/chibb-socket
```
Stel een topic beschikbaar waar de UI zich kan abonneren om berichten te ontvangen:
```
/topic/temperatures
```

# Getting Started

**Vereisten**
Om de applicatie moet er eest aantal services geinstalleerd en gestart worden. Hieronder zie jij welke services het zijn en op welke poorten zij gestart moeten worden.
 - MQTT Broker - Port: 1883
 - Mongo DB - Port: 27017

Installeren
  - Kloon BitBucket repository
  - Pas de application.properties van de microservices aan. Geef de locaties door van waar de bovenstaande services 

**Tools**
 - Spring Boot
 - Maven - Dependency Management
 - MQTT - Sensor data berichten distribueren
 - MongoDB - Data opslag
 - Angular 2 - Web Applicatie 
 - Typescript - Alternatief voor Javascript
 - HighCharts - Javascript library voor interactieve grafieken

**Deployment Pipeline**

![N|Solid](https://lh3.googleusercontent.com/CE9YIp8XKTz-uCRnryFSGG11DugT14ew8MP7jBy2bJdRLELbItF4uXAI0IFRbE90fpGX_nD0XebHEkw=w1360-h659)

